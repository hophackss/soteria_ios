//
//  main.m
//  Soteria
//
//  Created by Aditya Patil on 2/5/16.
//  Copyright © 2016 HopHacksSpring16. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
