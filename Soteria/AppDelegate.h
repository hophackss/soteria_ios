//
//  AppDelegate.h
//  Soteria
//
//  Created by Aditya Patil on 2/5/16.
//  Copyright © 2016 HopHacksSpring16. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

