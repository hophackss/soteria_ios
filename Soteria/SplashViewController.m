//
//  SplashViewController.m
//  Soteria
//
//  Created by Aditya Patil on 2/6/16.
//  Copyright © 2016 HopHacksSpring16. All rights reserved.
//

#import "SplashViewController.h"
#import "Constants.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    
    [defaults synchronize];
    if ([defaults boolForKey:HasLaunchedOnce])
    {
        // app already launched
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else
    {
        // This is the first launch ever
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"FormVC"];
        [self.navigationController pushViewController:vc animated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
