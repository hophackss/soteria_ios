//
//  Constants.h
//  Soteria
//
//  Created by Aditya Patil on 2/6/16.
//  Copyright © 2016 HopHacksSpring16. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define defaults [NSUserDefaults standardUserDefaults]

static NSString * const HasLaunchedOnce = @"HasLaunchedOnce";


#endif /* Constants_h */
