//
//  FormViewController.h
//  Soteria
//
//  Created by Aditya Patil on 2/6/16.
//  Copyright © 2016 HopHacksSpring16. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, assign) float horizontalPadding;

@end
