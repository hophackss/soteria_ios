//
//  ViewController.h
//  Soteria
//
//  Created by Aditya Patil on 2/5/16.
//  Copyright © 2016 HopHacksSpring16. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewTrack;
@property (strong, nonatomic) IBOutlet UIView *viewCall;
@property (strong, nonatomic) IBOutlet UIView *viewEmergency;

@property (strong, nonatomic) IBOutlet UIImageView *imgEmergency;

@end

