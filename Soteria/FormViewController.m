//
//  FormViewController.m
//  Soteria
//
//  Created by Aditya Patil on 2/6/16.
//  Copyright © 2016 HopHacksSpring16. All rights reserved.
//

#import "FormViewController.h"
#import "Constants.h"

@interface FormViewController ()

@end

@implementation FormViewController {

    CGRect screenRect;
    CGFloat screenWidth;
    CGFloat screenHeight;
    
    NSUserDefaults *userDefaults;
    
    NSMutableArray *arrHeight;
    
    UIPageControl *pageControl;
    int currentPage;
    
    UIView *baseView;
    UIView *screen1;
    UIView *screen2;
    UIView *screen3;
    UIView *screen4;
    UIView *screen5;
    
    UIView *baseBlurView;
    
    UITextField *txtEmail;
    UITextField *txtFName;
    UITextField *txtLastName;
    
    UITextField *txtAge;
    UIButton *btnGender;
    UIButton *btnHeight;
    UIView *viewPickerHeight;
    UIPickerView *pickerHeight;
    NSString *strHeight;
    UIVisualEffectView *bluredView;

    UITextField *txtAddress1;
    UITextField *txtAddress2;
    UITextField *txtCity;
    UITextField *txtState;
    UITextField *txtPhone;
    UITextField *txtEmergencyName;
    UITextField *txtEmergencyPhone;
}

@synthesize horizontalPadding;

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];

    [defaults synchronize];
    if ([defaults boolForKey:HasLaunchedOnce])
    {
        // app already launched - means user is coming from home screen
        self.navigationController.navigationBar.hidden = NO;

//        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
//        [self.navigationController pushViewController:vc animated:NO];
    }
    else
    {
        // This is the first launch ever
        
        userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[NSString stringWithFormat:@"Email"] forKey:@"Email"];
        [userDefaults setObject:[NSString stringWithFormat:@"FName"] forKey:@"FName"];
        [userDefaults setObject:[NSString stringWithFormat:@"LName"] forKey:@"LName"];
        [userDefaults setObject:[NSString stringWithFormat:@"Age"] forKey:@"Age"];
        [userDefaults setObject:[NSString stringWithFormat:@"Gender"] forKey:@"Gender"];
        [userDefaults setObject:[NSString stringWithFormat:@"Height"] forKey:@"Height"];
        [userDefaults setObject:[NSString stringWithFormat:@"Race"] forKey:@"Race"];
        [userDefaults setObject:[NSString stringWithFormat:@"Address1"] forKey:@"Address1"];
        [userDefaults setObject:[NSString stringWithFormat:@"Address2"] forKey:@"Address2"];
        [userDefaults setObject:[NSString stringWithFormat:@"City"] forKey:@"City"];
        [userDefaults setObject:[NSString stringWithFormat:@"State"] forKey:@"State"];
        [userDefaults setObject:[NSString stringWithFormat:@"Phone"] forKey:@"Phone"];
        [userDefaults setObject:[NSString stringWithFormat:@"EmergencyName"] forKey:@"EmergencyName"];
        [userDefaults setObject:[NSString stringWithFormat:@"EmergencyPhone"] forKey:@"EmergencyPhone"];
        [userDefaults synchronize];

        self.navigationController.navigationBar.hidden = YES;
    }

    [[UIBarButtonItem appearance]setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(BarButtonLeft)];
    leftButton.image=[UIImage imageNamed:@"sidebar"];
    self.navigationItem.leftBarButtonItem = leftButton;

    screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
    NSMutableArray *myArray = [[NSArray arrayWithContentsOfFile:plistPath] copy];
    arrHeight = [[myArray objectAtIndex:0] copy];

    baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4*screenWidth, screenHeight)];
    baseView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    [self.view addSubview:baseView];

    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(screenWidth/2 - 25, screenHeight-30, 50, 30)];
    pageControl.numberOfPages = 4;
    currentPage = 0;
    pageControl.currentPage = currentPage;
    [self.view addSubview:pageControl];
    
    screen1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, baseView.frame.size.height - 40)];
    screen1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    [baseView addSubview:screen1];

    screen2 = [[UIView alloc] initWithFrame:CGRectMake(screenWidth, 0, screenWidth, baseView.frame.size.height - 40)];
    screen2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    [baseView addSubview:screen2];

    screen3 = [[UIView alloc] initWithFrame:CGRectMake(2*screenWidth, 0, screenWidth, baseView.frame.size.height - 40)];
    screen3.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    [baseView addSubview:screen3];

    screen4 = [[UIView alloc] initWithFrame:CGRectMake(3*screenWidth, 0, screenWidth, baseView.frame.size.height - 40)];
    screen4.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    [baseView addSubview:screen4];

    screen5 = [[UIView alloc] initWithFrame:CGRectMake(4*screenWidth, 0, screenWidth, baseView.frame.size.height - 40)];
    screen5.backgroundColor = [UIColor purpleColor];
    [baseView addSubview:screen5];

    UIPanGestureRecognizer *swipe1 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(methodSwipe1:)];
    UIPanGestureRecognizer *swipe2 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(methodSwipe2:)];
    UIPanGestureRecognizer *swipe3 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(methodSwipe3:)];
    UIPanGestureRecognizer *swipe4 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(methodSwipe4:)];
    
    [screen1 addGestureRecognizer:swipe1];
    [screen2 addGestureRecognizer:swipe2];
    [screen3 addGestureRecognizer:swipe3];
    [screen4 addGestureRecognizer:swipe4];
    
    [self setupScreen1];
    [self setupScreen2];
    [self setupScreen3];
    [self setupScreen4];
    [self setupScreen5];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)BarButtonLeft {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Setup Screens

- (UIButton *)setupBtnNext:(UIButton *)btn {

    btn = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth-80, 30, 80, 40)];
    btn.backgroundColor = [UIColor clearColor];
    [btn setTitle:@">" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:35.0];
    btn.titleLabel.textAlignment = NSTextAlignmentRight;
    [btn addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (UIButton *)setupBtnHome:(UIButton *)btn {
    btn = [[UIButton alloc] initWithFrame:CGRectMake(10, 35, 30, 30)];
    btn.backgroundColor = [UIColor clearColor];
    [btn setBackgroundImage:[UIImage imageNamed:@"home"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(home:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (UIButton *)setupBtnPrev:(UIButton *)btn {
    btn = [[UIButton alloc] initWithFrame:CGRectMake(5, 35, 40, 40)];
    btn.backgroundColor = [UIColor clearColor];
    [btn setTitle:@"<" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:35.0];
    btn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [btn addTarget:self action:@selector(previous:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (void)setupScreen1 {
    
    UIButton *btnNext;
    [screen1 addSubview:[self setupBtnNext:btnNext]];
    
    if ([defaults boolForKey:HasLaunchedOnce])
    {
        UIButton *btnHome;
        [screen1 addSubview:[self setupBtnHome:btnHome]];
    }

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(30, 100, screenWidth-60, 230)];
    view.backgroundColor = [UIColor clearColor];
    [screen1 addSubview:view];
    
    txtEmail = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 50)];
    txtEmail.keyboardType = UIKeyboardTypeEmailAddress;
    [self formatTextField:txtEmail :@"Email"];
    [view addSubview:txtEmail];
    
    txtFName = [[UITextField alloc] initWithFrame:CGRectMake(0, 80, view.frame.size.width, 50)];
    [self formatTextField:txtFName :@"Name"];
    [view addSubview:txtFName];

    txtLastName = [[UITextField alloc] initWithFrame:CGRectMake(0, 160, view.frame.size.width, 50)];
    [self formatTextField:txtLastName :@"Last Name"];
    [view addSubview:txtLastName];
}

- (void)setupScreen2 {
    
    UIButton *btnNext;
    [screen2 addSubview:[self setupBtnNext:btnNext]];
    
    if ([defaults boolForKey:HasLaunchedOnce])
    {
        UIButton *btnPrev;
        [screen2 addSubview:[self setupBtnPrev:btnPrev]];
    }

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20, 100, screenWidth-40, 300)];
    view.backgroundColor = [UIColor clearColor];
    [screen2 addSubview:view];
    
    txtAge = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, screenWidth-40, 50)];
    txtAge.keyboardType = UIKeyboardTypeNumberPad;
    txtAge.tag = 101;
    txtAge.delegate = self;
    [self formatTextField:txtAge :@"Age"];
    [view addSubview:txtAge];
    
    btnGender = [[UIButton alloc] initWithFrame:CGRectMake(0, 80, screenWidth-40, 50)];
    [btnGender setTitle:@"   Gender" forState:UIControlStateNormal];
    [btnGender addTarget:self action:@selector(btnGenderMethod:) forControlEvents:UIControlEventTouchUpInside];
    [self formatButton:btnGender];
    [view addSubview:btnGender];
    
    btnHeight = [[UIButton alloc] initWithFrame:CGRectMake(0, 160, screenWidth-40, 50)];
    [btnHeight setTitle:@"   Height" forState:UIControlStateNormal];
    [btnHeight addTarget:self action:@selector(btnHeightMethod:) forControlEvents:UIControlEventTouchUpInside];
    [self formatButton:btnHeight];
    [view addSubview:btnHeight];
    
    UITextField *txtRace = [[UITextField alloc] initWithFrame:CGRectMake(0, 240, screenWidth-40, 50)];
    txtRace.delegate = self;
    [self formatTextField:txtRace :@"Race"];
    [view addSubview:txtRace];

}

- (void)setupScreen3 {
    
    UIButton *btnNext;
    [screen3 addSubview:[self setupBtnNext:btnNext]];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20, 100, screenWidth-40, 260)];
    view.backgroundColor = [UIColor clearColor];
    [screen3 addSubview:view];

    txtAddress1 = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, screenWidth-40, 50)];
    [self formatTextField:txtAddress1 :@"Address Line 1"];
    [view addSubview:txtAddress1];
    
    txtAddress2 = [[UITextField alloc] initWithFrame:CGRectMake(0, 70, screenWidth-40, 50)];
    [self formatTextField:txtAddress2 :@"Address Line 2"];
    [view addSubview:txtAddress2];

    txtCity = [[UITextField alloc] initWithFrame:CGRectMake(0, 140, screenWidth-100, 50)];
    [self formatTextField:txtCity :@"City"];
    [view addSubview:txtCity];
    
    txtState = [[UITextField alloc] initWithFrame:CGRectMake(screenWidth-90, 140, 50, 50)];
    [self formatTextField:txtState :@"ST"];
    txtState.keyboardType = UIKeyboardTypeAlphabet;
    [view addSubview:txtState];

    txtPhone = [[UITextField alloc] initWithFrame:CGRectMake(0, 210, screenWidth-40, 50)];
    [self formatTextField:txtPhone :@"Phone Number"];
    txtPhone.keyboardType = UIKeyboardTypeNumberPad;
    [view addSubview:txtPhone];
}

- (void)setupScreen4 {
    
    UIButton *btnNext;
    [screen4 addSubview:[self setupBtnNext:btnNext]];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20, 100, screenWidth-40, 260)];
    view.backgroundColor = [UIColor clearColor];
    [screen4 addSubview:view];
    
    txtEmergencyName = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, screenWidth-40, 50)];
    [self formatTextField:txtEmergencyName :@"Emergency Name"];
    [view addSubview:txtEmergencyName];
    
    txtEmergencyPhone = [[UITextField alloc] initWithFrame:CGRectMake(0, 70, screenWidth-40, 50)];
    txtEmergencyPhone.keyboardType = UIKeyboardTypeNumberPad;
    [self formatTextField:txtEmergencyPhone :@"Emergency Phone"];
    [view addSubview:txtEmergencyPhone];

}

- (void)setupScreen5 {
    
}

#pragma mark - Button Action

- (IBAction)btnStateMethod:(id)sender {
    
}

- (void)home:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)previous:(id)sender {
    
}

- (IBAction)next:(id)sender {
    
    if (currentPage == 0) {
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtEmail.text] forKey:@"Email"];
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtFName.text] forKey:@"FName"];
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtLastName.text] forKey:@"LName"];
    }
    else if (currentPage == 1) {
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtAge] forKey:@"Age"];
        //Height and Gender userDefaults set in their methods
        [userDefaults setObject:[NSString stringWithFormat:@"Race"] forKey:@"Race"];
    }
    else if (currentPage == 2) {
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtAddress1] forKey:@"Address1"];
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtAddress2] forKey:@"Address2"];
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtCity] forKey:@"City"];
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtState] forKey:@"State"];
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtPhone] forKey:@"Phone"];
    }
    else {
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtEmergencyName] forKey:@"EmergencyName"];
        [userDefaults setObject:[NSString stringWithFormat:@"%@", txtEmergencyPhone] forKey:@"EmergencyPhone"];
    }

    [userDefaults synchronize];
    
    if (currentPage == 3) {
        
        [defaults setBool:YES forKey:HasLaunchedOnce];
        [defaults synchronize];

        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        [UIView animateWithDuration:0.6 animations:^{
            baseView.frame = CGRectMake(baseView.frame.origin.x-screenWidth, baseView.frame.origin.y, baseView.frame.size.width, baseView.frame.size.width);
            pageControl.currentPage = ++currentPage;
        }completion:^(BOOL finished) {
        }];
    }

}

#pragma mark - Swipe Methods

- (void)methodSwipe1:(UIPanGestureRecognizer *)gesture {
    
}

- (void)methodSwipe2:(UIPanGestureRecognizer *)gesture {
    
}

- (void)methodSwipe3:(UIPanGestureRecognizer *)gesture {
    
}

- (void)methodSwipe4:(UIPanGestureRecognizer *)gesture {
    
}

#pragma mark - Textfield Delegate

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (IBAction)btnHeightMethod:(id)sender {

    [txtAge resignFirstResponder];

    baseBlurView = [[UIView alloc] initWithFrame:self.view.bounds];
    baseBlurView.alpha = 1.0;
    baseBlurView.backgroundColor = [UIColor clearColor];
    [screen2 addSubview:baseBlurView];
    
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    bluredView = [[UIVisualEffectView alloc] initWithEffect:effect];
    bluredView.frame = CGRectMake(0, screenHeight, screenWidth, 200);
    bluredView.alpha = 0.0;
    [baseBlurView addSubview:bluredView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDismissBlurHeight:)];
    [baseBlurView addGestureRecognizer:tap];
    
    viewPickerHeight = [[UIView alloc] initWithFrame:CGRectMake(0, 200, screenWidth, 200)];
    viewPickerHeight.backgroundColor = [UIColor clearColor];
    [bluredView addSubview:viewPickerHeight];
    
    [UIView animateWithDuration:0.6 animations:^{
        bluredView.alpha = 1.0;
        bluredView.frame = CGRectMake(0, screenHeight-200, screenWidth, 200);
        viewPickerHeight.frame = CGRectMake(viewPickerHeight.frame.origin.x, viewPickerHeight.frame.origin.y-viewPickerHeight.frame.size.height, viewPickerHeight.frame.size.width, viewPickerHeight.frame.size.height);
    } completion:^(BOOL finished) {
        
        UIButton *btnOK = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth-70, 0, 70, 30)];
        [btnOK setTitle:@"OK" forState:UIControlStateNormal];
        btnOK.backgroundColor = [UIColor clearColor];
        [btnOK addTarget:self action:@selector(btnOKPickerHeight:) forControlEvents:UIControlEventTouchUpInside];
        [viewPickerHeight addSubview:btnOK];
        
        pickerHeight = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 30, screenWidth, 150)];
        pickerHeight.delegate = self;
        pickerHeight.dataSource = self;
        pickerHeight.tintColor = [UIColor whiteColor];
        [viewPickerHeight addSubview:pickerHeight];
    }];
}

- (void)tapToDismissBlurHeight:(UITapGestureRecognizer *)gesture {
    [UIView animateWithDuration:0.6 animations:^{
        gesture.view.alpha = 0.0;
        bluredView.frame = CGRectMake(0, screenHeight, screenWidth, 200);
        viewPickerHeight.frame = CGRectMake(viewPickerHeight.frame.origin.x, viewPickerHeight.frame.origin.y+viewPickerHeight.frame.size.height, viewPickerHeight.frame.size.width, viewPickerHeight.frame.size.height);
    } completion:^(BOOL finished) {
        baseBlurView.alpha = 0.0;
        baseBlurView = nil;
    }];
}

- (IBAction)btnGenderMethod:(id)sender {
    
    [txtAge resignFirstResponder];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Gender"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *actionMale = [UIAlertAction actionWithTitle:@"Male" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [btnGender setTitle:@"Male" forState:UIControlStateNormal];
        [userDefaults setObject:[NSString stringWithFormat:@"Male"] forKey:@"Gender"];
    }];
    
    UIAlertAction *actionFemale = [UIAlertAction actionWithTitle:@"Female" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [btnGender setTitle:@"Female" forState:UIControlStateNormal];
        [userDefaults setObject:[NSString stringWithFormat:@"Female"] forKey:@"Gender"];
    }];
    
    UIAlertAction *actionOther = [UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [btnGender setTitle:@"Other" forState:UIControlStateNormal];
        [userDefaults setObject:[NSString stringWithFormat:@"Other"] forKey:@"Gender"];
    }];
    
    [alertController addAction:actionMale];
    [alertController addAction:actionFemale];
    [alertController addAction:actionOther];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)btnOKPickerHeight:(id)sender {
    
    [btnHeight setTitle:strHeight forState:UIControlStateNormal];
    [userDefaults setObject:[NSString stringWithFormat:@"Height"] forKey:@"Height"];

    [UIView animateWithDuration:0.6 animations:^{
        bluredView.alpha = 0.0;
        viewPickerHeight.frame = CGRectMake(viewPickerHeight.frame.origin.x, viewPickerHeight.frame.origin.y+viewPickerHeight.frame.size.height, viewPickerHeight.frame.size.width, viewPickerHeight.frame.size.height);
    } completion:^(BOOL finished) {
        baseBlurView.alpha = 0.0;
        baseBlurView = nil;
    }];
}


#pragma mark - Picker view delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [arrHeight count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arrHeight objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    strHeight = [arrHeight objectAtIndex:row];
}

#pragma mark - Format textfields

- (void)formatTextField:(UITextField *)txtField :(NSString *)strPlaceHolder{
    
    strPlaceHolder = [NSString stringWithFormat:@"   %@", strPlaceHolder];

    if ([txtField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor lightTextColor];
        txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strPlaceHolder attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
    }

    txtField.layer.cornerRadius = 10.0;
    txtField.clipsToBounds = YES;
    txtField.backgroundColor = [UIColor clearColor];
    txtField.layer.borderColor = [UIColor whiteColor].CGColor;
    txtField.layer.borderWidth = 1.0;
    txtField.textColor = [UIColor whiteColor];

}

- (void)formatButton:(UIButton *)btn {
 
    btn.layer.cornerRadius = 10.0;
    btn.clipsToBounds = YES;
    btn.backgroundColor = [UIColor clearColor];
    btn.layer.borderWidth = 1.0;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
}

@end
