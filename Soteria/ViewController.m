//
//  ViewController.m
//  Soteria
//
//  Created by Aditya Patil on 2/5/16.
//  Copyright © 2016 HopHacksSpring16. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController {
    
    UIImage *imgEmergency1;
    UIImage *imgEmergency2;
    int intBtnEmergencyBG;
    bool animateBtnEmergency;
    NSTimer *timer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.navigationController.navigationBar.hidden = NO;
    
    [[UIBarButtonItem appearance]setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];

    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(BarButtonLeft)];
    leftButton.image=[UIImage imageNamed:@"sidebar"];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    
    imgEmergency1 = [UIImage imageNamed:@"emergency_white"];
    imgEmergency2 = [UIImage imageNamed:@"emergency_red"];


    UITapGestureRecognizer *tapTrack = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTrackMethod:)];
    [self.viewTrack addGestureRecognizer:tapTrack];

    UITapGestureRecognizer *tapCall = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCallMethod:)];
    [self.viewCall addGestureRecognizer:tapCall];

    UITapGestureRecognizer *tapEmergency = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEmergencyMethod:)];
    [self.viewEmergency addGestureRecognizer:tapEmergency];

    animateBtnEmergency = false;
    intBtnEmergencyBG = 2;
    [self changeBtnEmergencyBG];

}

- (void)tapTrackMethod: (UITapGestureRecognizer *)gesture {
    
    
}

- (void)tapCallMethod: (UITapGestureRecognizer *)gesture {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Call"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *callNightRide = [UIAlertAction actionWithTitle:@"Night Ride" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Call Night Ride");
    }];
    
    UIAlertAction *callHopkinsSecurity = [UIAlertAction actionWithTitle:@"Hopkins Security" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Call HopkinsSecurity");
    }];
    
    UIAlertAction *call911 = [UIAlertAction actionWithTitle:@"911" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Call 911");
    }];
    
    UIAlertAction *callCustom = [UIAlertAction actionWithTitle:@"\"Custom\"" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Call Custom");
    }];
    
    UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL];
    
    [alertController addAction:callNightRide];
    [alertController addAction:callHopkinsSecurity];
    [alertController addAction:call911];
    [alertController addAction:callCustom];
    [alertController addAction:btnCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)tapEmergencyMethod: (UITapGestureRecognizer *)gesture {
    
    if (!animateBtnEmergency) {
        animateBtnEmergency = true;
        [self animateBtnEmergencyBG];
    }
    else {
        [timer invalidate];
        animateBtnEmergency = false;
        intBtnEmergencyBG = 2;
        [self changeBtnEmergencyBG];
    }
}

- (void)BarButtonLeft {
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"FormVC"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)animateBtnEmergencyBG {
        [timer invalidate];
        timer = [NSTimer scheduledTimerWithTimeInterval:0.57 target:self selector:@selector(changeBtnEmergencyBG) userInfo:NULL repeats:YES];
}

- (void)changeBtnEmergencyBG {
    
    if (intBtnEmergencyBG == 2) {
        self.imgEmergency.image = imgEmergency1;
        intBtnEmergencyBG = 1;
    }
    else {
        self.imgEmergency.image = imgEmergency2;
        intBtnEmergencyBG = 2;
    }
}

#pragma mark - action sheet delegat
@end
